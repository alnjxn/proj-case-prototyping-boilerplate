# **CASE JavaScript Prototyping Boilerplate**

(thanks to [Hackathon Starter](https://github.com/sahat/hackathon-starter))
-----
## **Dependencies**
* VirtualBox - https://www.virtualbox.org/wiki/Downloads
* Vagrant - https://www.vagrantup.com/downloads.html
* Node + NPM - http://nodejs.org/download/
* Git - http://git-scm.com/downloads

-----

## **Instructions (first time only)**

In terminal run the following:	

    git clone https://acdtprn@bitbucket.org/acdtprn/proj-case-prototyping-boilerplate.git case-boilerplate
    cd case-boilerplate
    vagrant up

Go and grab a coffee.  When it's finished:

    sudo npm install
    sudo npm install -g nodemon
    sudo nodemon app.js


## **Instructions (all other times)**

In terminal run the following:	

    cd case-boilerplate
    sudo nodemon app.js
_____

## **What you get**

* MySQL (port 3306)
* PostgreSQL (port 5432)
* MongoDB (port 27017)
* Neo4j (port 7474)
* Node (port 3000)
* Frontend Authentication using a Google account

_____

Node doesn't play nicely with Vagrant shared folders, so this is a hybrid solution.  House your project and run node on your local machine, run all other services in a virtual machine.

_____

## **How do I create a new page?**
A more correct way to be to say "How do I create a new route". The main file `app.js` contains all the routes.
Each route has a callback function associated with it. Sometimes you will see 3 or more arguments
to routes. In cases like that, the first argument is still a URL string, while middle arguments
are what's called middleware. Think of middleware as a door. If this door prevents you from
continuing forward, you won't get to your callback function. One such example is a route that requires authentication.

```js
app.get('/account', passportConf.isAuthenticated, userController.getAccount);
```

It always goes from left to right. A user visits `/account` page. Then `isAuthenticated` middleware
checks if you are authenticated:

```js
exports.isAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
};
```

If you are authenticated, you let this visitor pass through your "door" by calling `return next();`. It then proceeds to the
next middleware until it reaches the last argument, which is a callback function that typically renders a template on `GET` requests or redirects on `POST` requests. In this case, if you are authenticated, you will be redirected to *Account Management* page, otherwise you will be redirected to *Login* page.

```js
exports.getAccount = function(req, res) {
  res.render('account/profile', {
    title: 'Account Management'
  });
};
```

Express.js has `app.get`, `app.post`, `app.put`, `app.delete`, but for the most part you will only use the first two HTTP verbs, unless you are building a RESTful API.
If you just want to display a page, then use `GET`, if you are submitting a form, sending a file then use `POST`.

Here is a typical workflow for adding new routes to your application. Let's say we are building
a page that lists all books from database.

**Step 1.** Start by defining a route.
```js
app.get('/books', bookController.getBooks);
```

---

**Note:** As of Express 4.0 you can define you routes like so:

```js
app.route('/books')
  .get(bookController.getBooks)
  .post(bookController.createBooks)
  .put(bookController.updateBooks)
  .delete(bookController.deleteBooks)
```

And here is how a route would look if it required an *authentication* and an *authorization* middleware:

```js
app.route('/api/twitter')
  .all(passportConf.isAuthenticated)
  .all(passportConf.isAuthorized)
  .get(apiController.getTwitter);
  .post(apiController.postTwitter)
```

Use whichever style that makes sense to you. Either one is acceptable. I really think that chaining HTTP verbs on
`app.route` is very clean and elegant approach, but on the other hand I can no longer see all my routes at a glance
when you have one route per line.

---

**Step 2.** Create a new controller file called `book.js`.
```js
/**
 * GET /books
 * List all books.
 */

exports.getBooks = function(req, res) {
  Book.find(function(err, docs) {
    res.render('books', { books: docs });
  });
};
```

**Step 3.** Import that controller in `app.js`.
```js
var bookController = require('./controllers/book');
```

**Step 4.** Create `books.jade` template.
```jade
extends layout

block content
  .page-header
    h3 All Books

  ul
    for book in books
      li= book.name
```

That's it! I will say that you could have combined Step 1, 2, 3 as following:

```js
app.get('/books', function(req, res) {
  Book.find(function(err, docs) {
    res.render('books', { books: docs });
  });
});
```